#!/bin/bash

npm init -y
npm i express --save
npm i mocha chai --save-dev

mkdir src
mkdir test
mkdir public

touch public/index.html
touch src/main.js
touch src/mylib.js
touch test/mylib.test.js
touch README.md

echo -e "node_modules" > .gitignore

