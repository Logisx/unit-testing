// src/main.js

const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib');

app.get('/', (req, res) => {
  res.send('Hello World!');
});

// Endpoint to perform addition
app.get('/add', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const result = mylib.sum(a, b);
  res.send(result.toString());
});

// Endpoint to perform subtraction
app.get('/subtract', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const result = mylib.subtract(a, b);
  res.send(result.toString());
});

// Endpoint to perform multiplication
app.get('/multiply', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const result = mylib.multiply(a, b);
  res.send(result.toString());
});

// Endpoint to perform division
app.get('/divide', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  try {
    const result = mylib.divide(a, b);
    res.send(result.toString());
  } catch (error) {
    res.status(400).send(error.message);
  }
});

app.listen(port, () => {
  console.log(`Server: http://localhost:${port}`);
});
