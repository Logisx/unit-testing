// src/mylib.js

module.exports = {
    // Function to perform addition
    sum: (a, b) => a + b,
  
    // Function to perform subtraction
    subtract: (a, b) => a - b,
  
    // Function to perform multiplication
    multiply: (a, b) => a * b,
  
    // Function to perform division with error handling for division by zero
    divide: (a, b) => {
      // Check if the divisor (b) is zero
      if (b === 0) {
        // If the divisor is zero, throw an error
        throw new Error("ZeroDivision: Division by zero is not allowed.");
      }
      // Return the result of division
      return a / b;
    },
  };