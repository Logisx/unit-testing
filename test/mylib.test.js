// src/mylib.js

const expect = require("chai").expect;
const { should } = require("chai");
const mylib = require("../src/mylib");

// This is the main test suite for mylib.js
describe("Unit testing mylib.js", () => {
  let myvar = undefined;

  // This function will run before any tests in this suite
  before(() => {
    myvar = 9;
    console.log(`Before mylib tests: myvar = ${myvar}`);
  });

  // Test for the existence of myvar
  it("myvar should exist", () => {
    should().exist(myvar);
  });

  // Test suite for the 'sum' function
  describe("sum function", () => {
    // Test for addition
    it("should return 10 when using sum function with a=myvar, b=1", () => {
      const result = mylib.sum(myvar, 1);
      expect(result).to.equal(10);
    });

    // Test for subtraction
    it("should return 0 when using sum function with a=myvar, b= -9", () => {
      const result = mylib.sum(myvar, -9);
      expect(result).to.equal(0);
    });
  });

  // Test suite for the 'subtract' function
  describe("subtract function", () => {
    // Test for subtraction
    it("should return 8 when using subtract function with a=myvar, b=1", () => {
      const result = mylib.subtract(myvar, 1);
      expect(result).to.equal(8);
    });

    // Test for addition
    it("should return 10 when using subtract function with a=myvar, b= -1", () => {
      const result = mylib.subtract(myvar, -1);
      expect(result).to.equal(10);
    });
  });

  // Test suite for the 'multiply' function
  describe("multiply function", () => {
    // Test for multiplication
    it("should return 18 when using multiply function with a=myvar, b=2", () => {
      const result = mylib.multiply(myvar, 2);
      expect(result).to.equal(18);
    });

    // Test for multiplication by zero
    it("should return 0 when using multiply function with a=myvar, b=0", () => {
      const result = mylib.multiply(myvar, 0);
      expect(result).to.equal(0);
    });
  });

  // Test suite for the 'divide' function
  describe("divide function", () => {
    // Test for division
    it("should return 4.5 when using divide function with a=myvar, b=2", () => {
      const result = mylib.divide(myvar, 2);
      expect(result).to.equal(4.5);
    });

    // Test for division by zero
    it("should throw an error when dividing by zero", () => {
      expect(() => mylib.divide(myvar, 0)).to.throw('ZeroDivision');
    });
  });

  // This function will run after all tests in this suite have completed
  after(() => {
      myvar = undefined; // Reset myvar to undefined after tests
      console.log("After mylib tests: myvar = undefined");
  });
});
