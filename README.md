# Arithmetic Operations with Express and Testing

This project demonstrates basic arithmetic operations implemented in JavaScript using an Express.js server. It also includes unit tests using Mocha and Chai to ensure the correctness of the arithmetic functions. The project follows best practices for setting up a Node.js project, writing unit tests, and creating a simple API.

## Project Structure

- `commands.sh`: Initializes the project, install all dependencies.

- `src/mylib.js`: Contains basic arithmetic operations (addition, subtraction, multiplication, and division) along with error handling for division by zero.

- `src/main.js`: A sample main program that uses the `mylib` module to perform arithmetic operations.

- `test/mylib.test.js`: Unit tests for the `mylib` module using Mocha and Chai.

- `package-lock.json`: File ensures consistent and secure package installations by specifying exact versions of dependencies.
  
- `package.json`: Node.js project configuration file.

- `.gitignore`: Specifies files and directories to be ignored by Git.


## Setup and Usage

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/Logisx/unit-testing.git
2. Install project dependencies:

    ```bash
    . commands.sh

3. Run the server:
   
   ```bash
   node src/main.js
- The server will start on http://localhost:3000.

4. Access the API endpoints:

- Addition: http://localhost:3000/add?a=5&b=3
- Subtraction: http://localhost:3000/subtract?a=5&b=3
- Multiplication: http://localhost:3000/multiply?a=5&b=3
- Division: http://localhost:3000/divide?a=10&b=2

5. Run unit tests:

    ```bash
    npm run test
## API Endpoints
- **'/add'**: Performs addition. Pass a and b as query parameters.

- **'/subtract'**: Performs subtraction. Pass a and b as query parameters.

- **'/multiply'**: Performs multiplication. Pass a and b as query parameters.

- **'/divide'**: Performs division. Pass a and b as query parameters. Handles division by zero.

## Unit Tests
The **'test'** directory contains unit tests for the **'mylib'** module. These tests cover various scenarios, including error handling for division by zero.
